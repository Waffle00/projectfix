package com.example.latihan1.registerActivity

import android.view.View

class RegisterPresenter(private val rConnect : RegisterConnect) {
    fun getvalidregister(
        rU: String, rE: String, rPh :String, rUn: String, rP: String, rCp: String,
        eR: String, name: String, email: String, eP: String?, nP: String?
    ) {

        var msgUser = ""
        var msgEmail = ""
        var msgPhone = ""
        var msgUniv = ""
        var msgPass = ""
        var msgConfirm = ""
        var error = ""
        if (rU.isEmpty() || rU.isBlank()) {
            msgUser = "Username Kosong"
            rConnect.regiserroruser(msgUser)
            return
        }
        if (rE.isEmpty() || rE.isBlank()) {
            msgEmail = "Email Kosong"
            rConnect.regiserroremail(msgEmail)
            return
        }
        if (!rE.trim().matches(eR.toRegex())) {
            msgEmail = "itu bukan email"
            rConnect.regiserroremail(msgEmail)
            return
        }
        if (rUn.isEmpty() || rU.isBlank()) {
            msgUniv = "INstansi masih kosong"
            rConnect.regiserrorinstansi(msgUniv)
            return
        }
        if (rPh.isEmpty() || rPh.isBlank()) {
            msgPhone = "Nomer telp kosong"
            rConnect.regiserrorphone(msgPhone)
            return
        }
        if (rP.length < 6) {
            msgPass = "password kurang dari 6 karakter"
            rConnect.regiserrorpass(msgPass)
            return
        }
        if (rCp != rP) {
            msgConfirm = "password tidak sama"
            rConnect.regisconfirm(msgConfirm)
            return
        }
        if (name.equals(eP) && email.equals(nP)) {
            error = "akun sudah ada"
            rConnect.regiserror(error)
            return
        }

        rConnect.regissuccess()
    }
}