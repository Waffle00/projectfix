package com.example.latihan1.registerActivity

interface RegisterConnect {
    fun regiserroruser(msgUser:String)
    fun regiserroremail(msgEmail : String)
    fun regiserrorphone(msgPhone : String)
    fun regiserrorinstansi(msgUniv : String)
    fun regiserrorpass(msgPass : String)
    fun regisconfirm(msgConfirm : String)
    fun regiserror(error : String)
    fun regissuccess()
}