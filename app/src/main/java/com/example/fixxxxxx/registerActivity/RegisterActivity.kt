package com.example.latihan1.registerActivity

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.fixxxxxx.R
import com.example.fixxxxxx.Values.Sharedpref
import com.example.latihan1.loginActivity.LoginActivity
import kotlinx.android.synthetic.main.alertregister.*
import kotlinx.android.synthetic.main.register_activity.*

class RegisterActivity : AppCompatActivity(), RegisterConnect {

    private lateinit var registerpresenter : RegisterPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.register_activity)
        regbutton()

    }


    private fun regbutton() {
        btn_signup.setOnClickListener {
            val sharedPreference: Sharedpref = Sharedpref(this)
            val regisUsername: String = et_usernameregis.text.toString()
            val regisEmail: String = et_emailregis.text.toString()
            val regisPhone: String = et_phoneregis.text.toString()
            val regisUniv: String = et_instansiregis.text.toString()
            val regisPass: String = et_passregis.text.toString()
            val regisConfirmpass: String = et_confirmpass.text.toString()
            val emailRegex = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
            val name = et_usernameregis.editableText.toString()
            val email = et_emailregis.editableText.toString()
            val emailPref = sharedPreference.getValueString1("email")
            val namePref = sharedPreference.getValueString("name")

            registerpresenter = RegisterPresenter(this)
            registerpresenter.getvalidregister(regisUsername, regisEmail, regisPhone,regisUniv, regisPass, regisConfirmpass,
            emailRegex, name, email, emailPref, namePref)


        }

    }

    override fun regiserroruser(msgUser : String) {
        et_usernameregis.requestFocus()
        et_usernameregis.error = msgUser
    }

    override fun regiserroremail(msgEmail : String) {
        et_emailregis.requestFocus()
        et_emailregis.error = msgEmail
    }

    override fun regiserrorphone(msgPhone: String) {
        et_phoneregis.requestFocus()
        et_phoneregis.error = msgPhone
    }

    override fun regiserrorinstansi(msgUniv : String) {
        et_instansiregis.requestFocus()
        et_instansiregis.error = msgUniv
    }

    override fun regiserrorpass(msgPass : String) {
        et_instansiregis.requestFocus()
        et_instansiregis.error = msgPass
    }

    override fun regisconfirm(msgconfirm:String) {
        et_confirmpass.requestFocus()
        et_confirmpass.error = msgconfirm
    }

    override fun regiserror(error : String) {
        Toast.makeText(this, error , Toast.LENGTH_SHORT).show()
    }

    override fun regissuccess() {
        val sharedPreference: Sharedpref = Sharedpref(this)
        val name = et_usernameregis.editableText.toString()
        val email = et_emailregis.editableText.toString()
        val phone = et_phoneregis.editableText.toString()
        val instansi = et_instansiregis.editableText.toString()
        val pass = et_passregis.editableText.toString()
        sharedPreference.name("name", name)
        sharedPreference.save("email", email)
        sharedPreference.instansi("instansi",instansi)
        sharedPreference.phone("phone",phone)
        sharedPreference.pass("pass",pass)

        val dialogview = LayoutInflater.from(this).inflate(R.layout.alertregister, null)
        val mbuilderdialog = AlertDialog.Builder(this).setView(dialogview).setTitle("Apakah semua data sudah benar?")

        val alertdialog = mbuilderdialog.show()

        alertdialog.btn_sudahlogin.setOnClickListener{
            val mybutton = Intent(this@RegisterActivity, LoginActivity::class.java)
            startActivity(mybutton)
        }



    }
}