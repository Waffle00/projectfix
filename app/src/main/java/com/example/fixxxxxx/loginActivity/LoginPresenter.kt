package com.example.latihan1.loginActivity

class LoginPresenter(private val connect : LoginConnect) {
    fun getdataforvalid(eS:String, pS:String, eSP:String?, pSP:String?, eR:String, eE:String, pE:String) {
        var msg = ""
        var msg2 = ""
        var error = ""
        if (eS.isEmpty() || eS.isBlank()) {
            msg = "email kosong"
            connect.loginerrortextemail(msg)
            return
        }
        if (!eS.trim().matches(eR.toRegex())) {
            msg = "email tidak sesuai"
            connect.loginerrortextemail(msg)
            return
        }
        if (pS.isEmpty() || pS.isBlank()) {
            msg2 = "password kosong"
            connect.loginerrortextpass(msg2)
            return
        }
        if (!eE.equals(eSP) && !pE.equals(pSP)) {
            error = "email atau pass tidak sesuai"
            connect.loginerror(error)
            return
        }

            connect.loginsuccess()

    }
}