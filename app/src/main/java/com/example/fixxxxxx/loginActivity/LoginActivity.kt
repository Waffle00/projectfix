package com.example.latihan1.loginActivity

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.fixxxxxx.NaviActivity
import com.example.fixxxxxx.R
import com.example.fixxxxxx.Values.Sharedpref
import com.example.latihan1.registerActivity.RegisterActivity
import kotlinx.android.synthetic.main.login_activity.*

class LoginActivity : AppCompatActivity(), LoginConnect {


   lateinit var presenter: LoginPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.login_activity)
        signupbutton()
        textwajib()
    }

    fun signupbutton() {

        val textView: TextView = findViewById(R.id.btn_intentregis) as TextView
        textView.setOnClickListener {
            val mybutton = Intent(this@LoginActivity, RegisterActivity::class.java)
            startActivity(mybutton)
        }
    }


    fun textwajib() {
         btn_signin.setOnClickListener {
            val emailRegex = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
            val sharedPreference: Sharedpref = Sharedpref(this)
            val emailEdit = et_loginemail.editableText.toString()
            val passEdit = et_loginpass.editableText.toString()
            val emailSharedpref = sharedPreference.getValueString1("email")
            val passSharedPref = sharedPreference.getValueString4("pass")
            val emailString = et_loginemail.text.toString()
            val passString = et_loginemail.text.toString()



            presenter = LoginPresenter(this)
            presenter.getdataforvalid(emailString, passString,emailSharedpref ,passSharedPref, emailRegex, emailEdit, passEdit)


        }
    }

    override fun loginerrortextemail(msg: String) {
        et_loginemail.requestFocus()
        et_loginemail.error = msg
    }

    override fun loginerrortextpass(msg2: String) {
        et_loginpass.requestFocus()
        et_loginpass.error = msg2
    }

    override fun loginerror(msg: String) {
        Toast.makeText(this, msg , Toast.LENGTH_SHORT).show()
    }

    override fun loginsuccess() {
        val sharedPreference: Sharedpref = Sharedpref(this)
        val emailEdit = et_loginemail.editableText.toString()
        val passEdit = et_loginpass.editableText.toString()
        sharedPreference.aktifemail("aktifemail", emailEdit)
        sharedPreference.aktifpass("aktifpass", passEdit)
        val mybutton = Intent(this@LoginActivity, NaviActivity::class.java)
        startActivity(mybutton)
    }

    override fun onStart() {
        super.onStart()

        val sharedPreference: Sharedpref = Sharedpref(this)
        val gogo1 = sharedPreference.getValueString5("aktifemail")
        val gogo2 = sharedPreference.getValueString6("aktifpass")


        if (gogo1 != null && gogo2 != null) {
            val dadu = Intent(this@LoginActivity, NaviActivity::class.java)
            startActivity(dadu)
            finish()

        }
    }

}
/*fun forshare() {
    val sharedPreferences = getSharedPreferences("SP_INFO", Context.MODE_PRIVATE)

    val getemail = sharedPreferences.getString("EMAIL", "")
    val getpass = sharedPreferences.getString("PASS", "")
      val sharedPreference: SharedPref = SharedPref(this)






}*/
