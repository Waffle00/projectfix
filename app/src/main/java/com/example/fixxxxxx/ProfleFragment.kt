package com.example.fixxxxxx

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.fixxxxxx.Values.Sharedpref
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfleFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sharedPreference: Sharedpref = Sharedpref(requireContext())
        val namepref = sharedPreference.getValueString("name")
        val emailpref = sharedPreference.getValueString1("email")
        val phonepref = sharedPreference.getValueString2("phone")
        val instansi = sharedPreference.getValueString3("instansi")

        tv_nameprofile.text = namepref
        tv_emailprofile.text = emailpref
        tv_phoneprofile.text = phonepref
        tv_instansi.text = instansi

    }
}