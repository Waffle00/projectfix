package com.example.fixxxxxx

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.example.fixxxxxx.Values.Sharedpref
import com.example.latihan1.homeActivity.HomeFragment
import com.example.latihan1.loginActivity.LoginActivity
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.appbar.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.home_activity.*
import kotlinx.android.synthetic.main.home_header.*

class NaviActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)

        actionbar()
        displayScreen(-1)
        nameHeader()


    }

    private fun actionbar() {
        val navBar = ActionBarDrawerToggle(
            Activity(),
            drawer_layout,
            toolbar_rcy,
            R.string.nav_open,
            R.string.nav_close
        )
        drawer_layout.addDrawerListener(navBar)
        navBar.syncState()
        nav_navi.setNavigationItemSelectedListener(this)

    }

    fun displayScreen(id : Int) {
        val fragment = when (id) {
            R.id.nav_profile -> {
                ProfleFragment()
            }
            R.id.nav_androver -> {
                HomeFragment()
            }
            else -> {
                ProfleFragment()
            }
        }
        supportFragmentManager.beginTransaction().replace(R.id.layer, fragment).commit()

        when(id) {
            R.id.nav_logout -> {
                logOut()
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
       displayScreen(item.itemId)
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    fun nameHeader() {
        val navigationView = nav_navi as NavigationView
        val headerView: View = navigationView.getHeaderView(0)
        val sharedPreference: Sharedpref = Sharedpref(this)
        val namepref = sharedPreference.getValueString("name")
        val emailpref = sharedPreference.getValueString1("email")
        headerView.findViewById<TextView>(R.id.tv_namenavi).text = namepref
        headerView.findViewById<TextView>(R.id.tv_emailnavi).text = emailpref

    }
    fun logOut() {
        val sharedPreference: Sharedpref = Sharedpref(this)
        sharedPreference.removeValue("aktifemail")
        sharedPreference.removeValue1("aktifpass")
        val mybutton = Intent(this@NaviActivity, LoginActivity::class.java)
        startActivity(mybutton)

    }


}