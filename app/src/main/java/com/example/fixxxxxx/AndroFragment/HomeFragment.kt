package com.example.latihan1.homeActivity

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.fixxxxxx.DetailFragment
import com.example.fixxxxxx.R


class HomeFragment : Fragment(), HomeView, HomeAdapter.setClick {
    private lateinit var rvAndro: RecyclerView
    private var list: ArrayList<Android> = arrayListOf()
    private lateinit var andropres: HomePresenter
    val dataAndro: AndroidData = AndroidData
    private lateinit var adapter: HomeAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.androver_fragment, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvAndro = view.findViewById(R.id.rv_home)
        rvAndro.setHasFixedSize(true)

        dataList()
    }


    fun showRecyclerList() {
        rvAndro.layoutManager = LinearLayoutManager(requireContext())
        adapter = HomeAdapter(list, this)
        rvAndro.adapter = adapter

    }

    fun dataList() {
        val andronem = dataAndro.androidName
        val androver = dataAndro.androidDetail
        val androphoto = dataAndro.androidPicture
        Log.d("andronem", andronem.toString())
        showRecyclerList()
        andropres = HomePresenter(this)
        andropres.getlistdata(andronem, androver, androphoto)

    }

    override fun showList(data: ArrayList<Android>) {
        list.clear()
        list.addAll(data)
        adapter.notifyDataSetChanged()
    }

    override fun setItemClick(listAndroid: Android, position: Int) {

        val fm: FragmentManager = fragmentManager!!
        val ft: FragmentTransaction = fm.beginTransaction()
        val tofragment = DetailFragment()
        ft.replace(R.id.layer, tofragment)
        ft.addToBackStack(HomeFragment::class.java.getSimpleName())
        ft.commit()

        val bundle = Bundle()
        bundle.putString("androidname", listAndroid.androName)
        bundle.putString("androiddetail", listAndroid.androDetail)
        bundle.putInt("andropicture", listAndroid.androPicture)
        tofragment.setArguments(bundle)

    }

}