package com.example.latihan1.homeActivity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.fixxxxxx.R

class HomeAdapter(private val listAndroid: ArrayList<Android> , var clickListener : setClick) :
    RecyclerView.Adapter<HomeAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.androver_recycleview, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listAndroid.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //val andro = listAndroid[position]
        //holder.tvandroidname.text = andro.androName
        //holder.tvandroiddetail.text = andro.androDetail
        //Glide.with(holder.itemView.context)
          //  .load(andro.androPicture)
           // .into(holder.imgviewandro)
        holder.forClick(listAndroid.get(position), clickListener)

    }

    inner class ViewHolder(inihome: View) : RecyclerView.ViewHolder(inihome) {
        var tvandroidname: TextView = itemView.findViewById(R.id.tv_androname)
        var tvandroiddetail: TextView = itemView.findViewById(R.id.tv_androdetail)
        var imgviewandro: ImageView = itemView.findViewById(R.id.img_andro)

        fun forClick(listAndroid: Android , action: setClick){
            tvandroidname.text = listAndroid.androName
            tvandroiddetail.text = listAndroid.androDetail
            imgviewandro.setImageResource(listAndroid.androPicture)

            itemView.setOnClickListener{
                action.setItemClick(listAndroid,adapterPosition)
            }

        }

    }

    interface setClick {
        fun setItemClick(listAndroid: Android, position: Int)
    }

}



